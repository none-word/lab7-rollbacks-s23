CREATE DATABASE database;
\c database
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));

INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO PLayer (username, balance) VALUES ('Bob', 200);

INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);

CREATE TABLE Inventory (username TEXT, product TEXT, UNIQUE(username, product), amount INT CHECK (amount >= 0 AND amount <= 100));