package com.example.lab7.service;

import com.example.lab7.repository.InventoryRepository;
import com.example.lab7.repository.PlayerRepository;
import com.example.lab7.repository.ShopRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Service
public class ServiceImpl {
    private final ShopRepository shopRepository;
    private final PlayerRepository playerRepository;
    private final InventoryRepository inventoryRepository;

    /**
     * @Transactional annotation ensures that all of these operations are executed as part of a single transaction.
     * All of the database operations are executed within the same transaction.
     * If any operation fails, the entire transaction is rolled back, ensuring that the data remains consistent.
     */
    @Transactional
    public void buyProduct(String username, String product, int amount) {
        Integer price = shopRepository.findPriceByProduct(product);

        int balanceUpdatedRows = playerRepository.updateBalanceByUsername(price, amount, username);
        if (balanceUpdatedRows != 1) {
            throw new RuntimeException("Wrong username");
        }

        int stockUpdatedRows = shopRepository.updateInStockByProduct(amount, product);
        if (stockUpdatedRows != 1) {
            throw new RuntimeException("Wrong product or out of stock");
        }

        int totalAmount = inventoryRepository.findAllByUsername(username).orElse(0);
        if (totalAmount + amount > 100) {
            throw new RuntimeException("Wrong total amount");
        }
        inventoryRepository.updateInStockByProduct(username, product, amount);
    }
}
