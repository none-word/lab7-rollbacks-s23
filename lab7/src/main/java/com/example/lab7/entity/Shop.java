package com.example.lab7.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Shop {
    @Id
    private String product;
    @Column(name = "in_stock")
    private Integer inStock;
    private Integer price;
}
