package com.example.lab7.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Player {
    @Id
    private String username;
    private Integer balance;
}
