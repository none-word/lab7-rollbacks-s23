package com.example.lab7.entity;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;

import java.io.Serializable;

@Entity
public class Inventory {

    @EmbeddedId
    private InventoryId inventoryId;

    private Integer amount;

    @Embeddable
    public static class InventoryId implements Serializable {
        private String username;
        private String product;
    }
}
