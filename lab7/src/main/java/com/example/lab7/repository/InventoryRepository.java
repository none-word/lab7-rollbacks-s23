package com.example.lab7.repository;

import com.example.lab7.entity.Inventory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface InventoryRepository extends JpaRepository<Inventory, Inventory.InventoryId> {

    @Modifying
    @Query(value = "INSERT INTO Inventory(username, product, amount) VALUES (:username, :product, :amount) ON CONFLICT (username, product) DO UPDATE SET amount = Inventory.amount + excluded.amount", nativeQuery = true)
    int updateInStockByProduct(@Param("username") String username, @Param("product") String product, @Param("amount") Integer amount);

    @Query("SELECT SUM(i.amount) FROM Inventory i where i.inventoryId.username = :username")
    Optional<Integer> findAllByUsername(@Param("username") String username);
}
