package com.example.lab7.repository;

import com.example.lab7.entity.Shop;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ShopRepository extends JpaRepository<Shop, String> {
    @Modifying
    @Query("UPDATE Shop s SET s.inStock = s.inStock - :amount WHERE s.product = :product")
    int updateInStockByProduct(@Param("amount") Integer amount, @Param("product") String product);

    @Query("SELECT s.price FROM Shop s WHERE s.product = :product")
    Integer findPriceByProduct(@Param("product") String product);
}
