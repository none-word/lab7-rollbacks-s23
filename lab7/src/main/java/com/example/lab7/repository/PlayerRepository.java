package com.example.lab7.repository;

import com.example.lab7.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface PlayerRepository extends JpaRepository<Player, String> {
    @Modifying
    @Transactional
    @Query("UPDATE Player p SET p.balance = p.balance - (:priceRequest * :amount) WHERE p.username = :username")
    int updateBalanceByUsername(@Param("priceRequest") Integer priceRequest, @Param("amount") Integer amount, @Param("username") String username);
}

