package com.example.lab7;

import com.example.lab7.service.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@RequiredArgsConstructor
@SpringBootApplication
public class Lab7Application {
    private final ServiceImpl service;

    public static void main(String[] args) {
        SpringApplication.run(Lab7Application.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void run() {
        service.buyProduct("Alice", "marshmello", 1);
    }
}
